package com.whitemagic2014.command.impl.group;

import com.whitemagic2014.pojo.CommandProperties;
import net.mamoe.mirai.contact.Group;
import net.mamoe.mirai.contact.Member;
import net.mamoe.mirai.message.data.At;
import net.mamoe.mirai.message.data.Message;
import net.mamoe.mirai.message.data.MessageChain;
import net.mamoe.mirai.message.data.PlainText;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Random;

@Component
public class EmptyCommand extends NoAuthCommand {
    @Override
    public CommandProperties properties() {
        return new CommandProperties("roll", "ROLL", "Roll");
    }

    @Override
    protected Message executeHandle(Member sender, ArrayList<String> args, MessageChain messageChain, Group subject) {
        Random random = new Random();
        if (!args.isEmpty() && args.size() != 1) {
            return new PlainText("指令错误: [指令前缀][roll] [数字(可省略)]");
        } else {
            At at = new At(sender);
            PlainText plainText = null;
            plainText = new PlainText(" roll出 ");
            return at.plus(plainText);
        }
    }
}
