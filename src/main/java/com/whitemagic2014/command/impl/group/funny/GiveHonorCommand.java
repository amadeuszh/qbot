package com.whitemagic2014.command.impl.group.funny;

import com.whitemagic2014.command.impl.group.NoAuthCommand;
import com.whitemagic2014.pojo.CommandProperties;
import net.mamoe.mirai.contact.Group;
import net.mamoe.mirai.contact.Member;
import net.mamoe.mirai.message.data.At;
import net.mamoe.mirai.message.data.Message;
import net.mamoe.mirai.message.data.MessageChain;
import net.mamoe.mirai.message.data.PlainText;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Random;

@Component
public class GiveHonorCommand extends NoAuthCommand {

    @Override
    public CommandProperties properties() {
        return new CommandProperties("授予称号");
    }

    @Override
    protected Message executeHandle(Member sender, ArrayList<String> args, MessageChain messageChain, Group subject) {
        System.out.println("sender: " + sender + "\nargs: " + args + "\nmessageChain: " + messageChain + "\nsubject: " + subject );
        if (!args.isEmpty() && args.size() != 2) {
            return new PlainText("指令错误: [指令前缀][授予称号] [被授予对象昵称] [称号]");
        } else {
            At at = new At(sender);
            PlainText plainText = null;
            String name = "";
            String honor = "";
            try {
                name = String.valueOf(args.get(0));
                honor = String.valueOf(args.get(1));
                System.out.println("name: " + name + " honor: " + honor);
            } catch (NumberFormatException e) {
                return new PlainText("指令错误: [指令前缀][授予称号] [被授予对象昵称] [称号]");
            }
            //根据名称查询对方qq

            //查询本地数据库里面是否已有该用户记录

            //更新记录
            plainText = new PlainText("该功能还未上线，建议一起开发");
            return at.plus(plainText);
        }
    }
}
